from pprint import pprint

from credit_db import CreditDb
from credit_ledger import CreditLedger
from util import get_future_datetime_utc


def seed(ledger: CreditLedger):
  ledger.add_user_credit(user_id=1, amount=20, expires_at=get_future_datetime_utc(days=362))
  ledger.add_user_credit(user_id=1, amount=-15, expires_at=None, created_at=get_future_datetime_utc(days=30))


if __name__ == '__main__':
  credit_db_inst = CreditDb()
  credit_ledger = CreditLedger(credit_db_inst)
  seed(credit_ledger)

  print('\n\n\n\nDb data:\n\n')
  pprint({k: v.to_dict() for k, v in credit_db_inst.get_all().items()})
  print(f'\n\n\n\nBalance in 1 year: {credit_ledger.get_user_balance(user_id=1, in_days=365)}\n\n\n\n')
