import datetime
import uuid
from typing import Optional

from util import datetime_to_string, get_current_datetime_utc


class Credit():

  def __init__(self,
               user_id: int,
               amount: float,
               created_at: Optional[datetime.datetime] = None,
               expires_at: Optional[datetime.datetime] = None):
    self.user_id = user_id
    self.amount = amount
    self.created_at = created_at or get_current_datetime_utc()
    self.credit_id = str(uuid.uuid4())
    self.expires_at = expires_at

  def to_dict(self):
    return {
      'credit_id': self.credit_id,
      'user_id': self.user_id,
      'amount': round(self.amount, 2),
      'expires_at': (datetime_to_string(self.expires_at)
                     if self.expires_at is not None
                     else None),
      'created_at': datetime_to_string(self.created_at),
    }
