import datetime
from typing import Optional

DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.00+00:00'


def get_current_datetime_utc() -> datetime.datetime:
  return datetime.datetime.now(tz=datetime.timezone.utc)


def get_future_datetime_utc(days: int) -> datetime.datetime:
  return get_current_datetime_utc() + datetime.timedelta(days=days)


def datetime_to_string(dt: datetime.datetime) -> Optional[str]:
  if dt:
    return dt.strftime(DATETIME_FORMAT)
  return None
